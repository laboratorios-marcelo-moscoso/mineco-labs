import { Component,inject } from '@angular/core';
import { ITarea } from '../tarea';
import { FormBuilder,Validators } from '@angular/forms';

@Component({
  selector: 'app-tareas',
  templateUrl: './tareas.component.html',
  styleUrls: ['./tareas.component.css']
})
export class TareasComponent {

  tareas : ITarea[];
  tarea : ITarea;
  openModal: string;

  dragData : any;

  private fb = inject(FormBuilder);

  form = this.fb.group(
    {
      titulo:['',[Validators.required]],
      descripcion:['',[Validators.required]],
      estado:['backlog',Validators.required]
    }
  );


  constructor(){
    this.tareas = [];
    this.tarea = { titulo:'',descripcion:'',estado:''};
    this.openModal = 'none';
  }

  onRegistarTarea(){
    if(this.form.valid){
      const { titulo,descripcion,estado } = this.form.getRawValue();
      this.tareas.push(this.form.getRawValue() as ITarea);
      this.form.reset();
      this.openModal = 'none';
      this.form.patchValue({estado:'backlog'});
    }
  }

  editarTarea(tar:ITarea){
    //this.tarea = { ...tar};
    this.form.patchValue(tar);
    this.openModal = 'block';
  }

  eliminarTarea(tar:ITarea){
    this.tareas = this.tareas.filter(task => task.id !== tar.id);
  }

  dragStart(event: DragEvent) {
    this.dragData = event.target;
  }

  allowDrop(event: DragEvent) {
    event.preventDefault();
  }

  drop(event: DragEvent) {
    event.preventDefault();
    const target = event.target as HTMLElement;
    target.appendChild(this.dragData);
    console.log(this.dragData);
  }


}
