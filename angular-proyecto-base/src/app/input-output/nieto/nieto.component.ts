import { Component,Input,Output,EventEmitter } from '@angular/core';

@Component({
  selector: 'app-nieto',
  templateUrl: './nieto.component.html',
  styleUrls: ['./nieto.component.css']
})
export class NietoComponent {

  @Input() ncontador:number;
  @Output() resetContador = new EventEmitter<number>();

  constructor(){
    this.ncontador = 10;
  }

  reset(){
    this.ncontador = 0;
    this.resetContador.emit(this.ncontador);
  }

}
