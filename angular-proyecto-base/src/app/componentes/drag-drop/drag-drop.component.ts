import { Component } from '@angular/core';

@Component({
  selector: 'app-drag-drop',
  templateUrl: './drag-drop.component.html',
  styleUrls: ['./drag-drop.component.css']
})
export class DragDropComponent {

  dragData : any;

  dragStart(event: DragEvent) {
    this.dragData = event.target;
  }

  allowDrop(event: DragEvent) {
    event.preventDefault();
  }

  drop(event: DragEvent) {
    event.preventDefault();
    const target = event.target as HTMLElement;
    target.appendChild(this.dragData);
    console.log(this.dragData);
  }


}
