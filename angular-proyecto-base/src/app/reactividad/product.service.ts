import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import {Product,ProductList } from './product';

@Injectable({
  providedIn: 'root'
})
export class ProductService {

  URL_API: string = 'https://dummyjson.com/products'; 

  constructor(private http: HttpClient) { }

  getProducts(){
    return this.http.get(this.URL_API);
  }

  addProduct(product:Product){
    return this.http.post(`${this.URL_API}/add`,product);
  }

  updateProduct(product:Product){
    return this.http.put(`${this.URL_API}/${product.id}`,product)
  }

  removeProduct(id:number){
    return this.http.delete(`${this.URL_API}/${id}`)
  }

}
