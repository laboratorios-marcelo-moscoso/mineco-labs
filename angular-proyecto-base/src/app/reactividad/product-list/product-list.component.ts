import { Component, OnInit } from '@angular/core';
import { ProductService } from '../product.service';
import { ProductList,Product } from '../product';

import { StoreService } from '../store.service';

@Component({
  selector: 'app-product-list',
  templateUrl: './product-list.component.html',
  styleUrls: ['./product-list.component.css']
})
export class ProductListComponent implements OnInit {

  productList : ProductList;
  products : Product[];

  constructor(
    private storeService: StoreService,
    private productService : ProductService){
    this.productList = { products:[] , total:0,skip:0,limit:0 };
    this.products = [];
  }

  ngOnInit(): void {
    this.loadProducts();
  }

  loadProducts(){
    this.productService.getProducts().subscribe(resp=>{
      this.productList = resp as ProductList;
      this.products = this.productList.products;
    })
  }

  addProductToCart(product:Product){
    this.storeService.addProduct(product);
  }

}
