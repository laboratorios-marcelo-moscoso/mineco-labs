import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { ProductList,Product} from './product';

@Injectable({
  providedIn: 'root'
})
export class StoreService {

  private myCart = new BehaviorSubject<Product[]>([]);
  private myProducts : Product[];
  myCart$ = this.myCart.asObservable();

  constructor() { 
    this.myProducts = [];
  }

  addProduct(product:Product){
    this.myProducts.push(product)
    this.myCart.next(this.myProducts);
  }

}
