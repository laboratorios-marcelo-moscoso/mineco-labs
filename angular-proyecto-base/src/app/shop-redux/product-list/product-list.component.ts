import { Component, OnInit } from '@angular/core';
import { ProductService } from '../service/product.service';
import { AppStateInterface, IProduct } from '../interfaces';
import { Store, select } from '@ngrx/store';
import * as CartActions from '../store/product.actions';
import { Observable } from 'rxjs';
import { cartSelector } from '../store/product.selectors';

@Component({
  selector: 'app-product-list',
  templateUrl: './product-list.component.html',
  styleUrls: ['./product-list.component.css']
})
export class ProductListComponent implements OnInit {

  cart$: Observable<IProduct[]>;
  products: IProduct[];

  constructor(
    private store: Store<AppStateInterface>,
    private productService: ProductService){
    this.products = [];
    this.cart$ = this.store.pipe(select(cartSelector));
  }

  ngOnInit(): void {
    this.productService.getProducts().then((data) => (this.products = data.slice(0, 12)));
  }

  addToCart(product:IProduct) {
    this.store.dispatch(CartActions.postCart({products: product}))
  }
  

}
