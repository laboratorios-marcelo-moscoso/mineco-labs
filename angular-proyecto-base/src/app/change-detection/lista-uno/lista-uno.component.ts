import { Component,EventEmitter,Output,ChangeDetectionStrategy  } from '@angular/core';

@Component({
  selector: 'app-lista-uno',
  templateUrl: './lista-uno.component.html',
  styleUrls: ['./lista-uno.component.css'],
  changeDetection:ChangeDetectionStrategy.Default
})
export class ListaUnoComponent {
  inputData : string;
  @Output() emitirLista = new EventEmitter<string>();

  constructor(){
    this.inputData = '';
  }

  onEnviarLista(){
    this.emitirLista.emit(this.inputData);
    this.inputData = '';
  }

  render():boolean{
    console.log('Render uno')
    return true;
  }

}
